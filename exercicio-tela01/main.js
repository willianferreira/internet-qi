var tipoQuarto;
var tipoCama;
var exigencias = [];

function adcValorRadio(event) {
	if(event.target.name == "tipoQuarto") {
		this.tipoQuarto = event.target.value;
	} else {
		this.tipoCama = event.target.value;
	}
}

function addExigencia(exigencia) {
	if(this.exigencias.includes(exigencia)) {
		this.exigencias = this.exigencias.filter(item => item != exigencia);
	} else {
		this.exigencias.push(exigencia);
	}
}

function exibirResultado() {
	event.preventDefault();
	$('#resultado').modal('show');
	let modal = document.querySelector(".modal-body");
	modal.innerHTML = this.montarResultado();
}

function montarResultado() {
	let valor = 0;
	this.tipoQuarto == "Suíte" ? valor += 160 : valor = 190; 
	this.exigencias.includes('Ar Condicionado') ? valor += 20 : valor += 0;
	let html = `
		<h5>Tipo de Quato</h5>
		<p>${this.tipoQuarto}</p>
		<h5>Tipo de Cama</h5>
		<p>${this.tipoCama}</p>
	`;

	if(this.exigencias.length > 0) {
		html += `<h5>Exigências</h5><p>`
		for(let i in this.exigencias) {
			html += `${this.exigencias[i]}`;
			if(i != this.exigencias.length -1) {
				html += `, `;
			} 		}
		html += `</p>`;
	}	

	html += `
		<h5>Preço Total</h5><p>
		<p>${valor}</p>
	`;
	return html;
}