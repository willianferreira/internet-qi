class Cadastro {

	constructor() {

	}

	limparCampos() {
        document.getElementById('rua').value=("");
        document.getElementById('bairro').value=("");
        document.getElementById('cidade').value=("");
        document.getElementById('complemento').value=("");
        document.getElementById('uf').value=("");
    }

    callback(conteudo) {
        if (!("erro" in conteudo)) {
            document.getElementById('rua').value=(conteudo.logradouro);
            document.getElementById('bairro').value=(conteudo.bairro);
            document.getElementById('cidade').value=(conteudo.localidade);
            document.getElementById('complemento').value=(conteudo.complemento);
            document.getElementById('uf').value=(conteudo.uf);
        } else {
            this.limparCampos();
            alert("CEP não encontrado.");
        }
    }


    pesquisarCep(valor) {

        let cep = valor.replace(/\D/g, '');

        if (cep != "") {

            let validacep = /^[0-9]{8}$/;

            if(validacep.test(cep)) {

                document.getElementById('rua').value="...";
                document.getElementById('bairro').value="...";
                document.getElementById('cidade').value="...";
                document.getElementById('complemento').value="...";
                document.getElementById('uf').value="...";

                let script = document.createElement('script');

                script.src = `https://viacep.com.br/ws/${cep}/json/?callback=cadastro.callback`;

                document.body.appendChild(script);

            } else {
                this.limparCampos();
                alert("Formato de CEP inválido.");
            }
        } else {
            this.limparCampos();
        }
    };


    cadastrar(formulario, event) {
        event.preventDefault();
        for (let div of formulario) {
            // console.log(div.value);
        }
    }
}