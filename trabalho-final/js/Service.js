class Service {

    constructor() {

    }


    getSabores () {
    	return [
			{ id: 1, nome: "Bacon" },
			{ id: 2, nome: "Frango" },
			{ id: 3, nome: "Calabresa" },
			{ id: 4, nome: "Strogonoff" },
			{ id: 5, nome: "Portuguesa" },
			{ id: 6, nome: "Picanha" },
			{ id: 7, nome: "Coração" }, 
			{ id: 8, nome: "Da casa" }
		]
	}

	getSaboresDoces() {
		return [
			{ id: 1, nome: "Chocolate" },
			{ id: 2, nome: "Chocolate Branco" },
			{ id: 3, nome: "Prestigio" },
			{ id: 4, nome: "Chokito" }
		]
	} 
	

}